# **EditeurTexte** #

EditeurTexte est une application qui permet d'éditer du texte réalisé en cours de première année de BTS SIO.

## Utilisation ##

Cette application permet de :

* Ouvrir un document texte 

* Modifier le texte du document

* Enregistrer le document

* Enregistrer sous

La taille de la zone de texte dépend de la taille de la fenêtre générale